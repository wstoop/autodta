from itertools import combinations

class BruteForce():

    def run(self,indices,distance_matrix):

        self.distance_matrix = distance_matrix

        #Make all clusters
        clusters = []
        for i in range(1,len(indices)):
            clusters+= list(combinations(indices,i))

        #Rate all clusters
        largest_cluster_size = max([len(cluster) for cluster in clusters])
        cluster_scores = {cluster: self.score_cluster(cluster,largest_cluster_size) for cluster in clusters}

        sorted_clusters = sorted(cluster_scores.items(),key=lambda x:x[1],reverse=True)

        #Find the exclusive clusters with the largest score
        exclusive_clusters = []
        indices_covered = []

        for cluster,score in sorted_clusters:

            cluster_is_completely_uncovered_yet = True
            for i in cluster:
                if i in indices_covered:
                    cluster_is_completely_uncovered_yet = False
                    break

            if not cluster_is_completely_uncovered_yet:
                continue
            else:
                if len(cluster) > 1:
                    exclusive_clusters.append(cluster)
                indices_covered+= cluster

        return exclusive_clusters

    def run_simple(self,indices,distance_matrix):

        self.distance_matrix = distance_matrix

        #Make all clusters
        clusters = []
        for i in range(1,len(indices)):
            clusters+= list(combinations(indices,i))

        #Rate all clusters
        largest_cluster_size = max([len(cluster) for cluster in clusters])
        cluster_scores = {cluster: self.score_cluster(cluster,largest_cluster_size) for cluster in clusters}

        sorted_clusters = sorted(cluster_scores.items(),key=lambda x:x[1],reverse=True)

        #Find the exclusive clusters with the largest score
        exclusive_clusters = []
        indices_covered = []

        for cluster,score in sorted_clusters:

            cluster_is_completely_uncovered_yet = True
            for i in cluster:
                if i in indices_covered:
                    cluster_is_completely_uncovered_yet = False
                    break

            if not cluster_is_completely_uncovered_yet:
                continue
            else:
                if len(cluster) > 1:
                    exclusive_clusters.append(cluster)
                indices_covered+= cluster

        return exclusive_clusters

    def score_cluster(self,cluster,largest_claster_size):

        if len(cluster) == 1:
            distance_score = 1
        else:
            all_pairs = combinations(cluster,2)
            distances = [self.get_distance_between_point(a,b) for a,b in all_pairs]
            distance_score = 1 - sum(distances) / len(distances)

        size_score = len(cluster) / largest_claster_size

        return 2 * (distance_score * size_score) / (distance_score + size_score)

    def get_distance_between_point(self, a, b):
        from_to = tuple(sorted([a, b]))
        return self.distance_matrix[from_to]
