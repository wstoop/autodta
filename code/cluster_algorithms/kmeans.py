from random import choice

class KMeans():

    max_nr_of_clusters = 8
    nr_of_clusters = 1
    iterations = 10

    distance_matrix = {}

    def __init__(self):

        pass

    def run(self, points, distance_matrix):

        self.distance_matrix = distance_matrix

        #Prevent more clusters than we have points
        if len(points) < self.max_nr_of_clusters:
            self.max_nr_of_clusters = len(points)

        #Try out multiple values for k
        previous_summed_distances_from_centroids = None
        previous_clusters = None

        for k in range(1,self.max_nr_of_clusters):
            self.nr_of_clusters = k

            centroids = self.pick_initial_centroids_from_points(points)

            for i in range(self.iterations):
                clusters = self.find_clusters_in_points_around_centroids(points, centroids)
                centroids = [self.find_centroid_of_cluster(cluster) for cluster in clusters]

            summed_distances_from_centroids = sum(self.find_distances_from_centroids(clusters,centroids))

            #Once the average distance from centroids does not go down anymore, return the clusters you found previously
            if previous_summed_distances_from_centroids != None and previous_summed_distances_from_centroids - summed_distances_from_centroids <= 1:
                return previous_clusters
            else:
                previous_clusters = clusters
                previous_summed_distances_from_centroids = summed_distances_from_centroids

        return clusters

    def pick_initial_centroids_from_points(self,points):

        centroids = [choice(points)]

        while len(centroids) < self.nr_of_clusters:

            point_most_far_away_from_centroids = None
            largest_distance = None

            for point in points:

                distance_to_centroids = 0

                for centroid in centroids:

                    if centroid != point:
                        distance_to_centroids += self.get_distance_between_point(centroid,point)

                if point_most_far_away_from_centroids == None or distance_to_centroids > largest_distance:
                    point_most_far_away_from_centroids = point
                    largest_distance = distance_to_centroids

            centroids.append(point_most_far_away_from_centroids)

        return centroids

    def find_clusters_in_points_around_centroids(self,points,centroids):

        clusters = [[] for i in range(self.nr_of_clusters)]

        for point in points:

            index_of_closest_centroid = None
            smallest_distance = None

            for n,centroid in enumerate(centroids):

                if point == centroid:
                    index_of_closest_centroid = n
                    break
                elif centroid == None:
                    continue
                else:
                    distance = self.get_distance_between_point(centroid,point)

                    if index_of_closest_centroid == None:
                        index_of_closest_centroid = n
                        smallest_distance = distance
                    elif distance < smallest_distance:
                        index_of_closest_centroid = n
                        smallest_distance = distance

            clusters[index_of_closest_centroid].append(point)

        return clusters

    def find_centroid_of_cluster(self,cluster):

        if len(cluster) == 0:
            return None
        elif len(cluster) == 1:
            return cluster[0]

        summed_distances = []

        for i in cluster:

            summed_distance = 0

            for j in cluster:
                if i == j:
                    continue

                summed_distance += self.get_distance_between_point(i,j)

            summed_distances.append(summed_distance)

        #returns the value that corresponds to the smallest value of summed_distances
        return cluster[summed_distances.index(min(summed_distances))]

    def find_distances_from_centroids(self,clusters,centroids):

        distances = []

        for n,cluster in enumerate(clusters):
            centroid = centroids[n]

            for point in cluster:

                if point != centroid:
                    distances.append(self.get_distance_between_point(point,centroid))

        return distances

    def get_distance_between_point(self,a,b):
        from_to = tuple(sorted([a, b]))
        return self.distance_matrix[from_to]