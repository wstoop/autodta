from typing import List, Dict, Tuple

class DBSCAN_distance_matrix():

    #Assumes matrix of the type {(0,1):123,(2,3):234}, where 0,1,3 are point indices, and 123 and 234 are distances

    maximum_intra_cluster_distance = .7 #original was .6
    minimum_cluster_size = 2

    points : List[int] = []
    distance_matrix : Dict[Tuple[int,int],float] = {}

    visited_points : List[int] = []
    points_already_in_cluster : List[int] = []
    clusters : List[List[int]] = []

    def __init__(self):
        self.points = []
        self.distance_matrix = {}

        self.visited_points = []
        self.points_already_in_cluster = []
        self.clusters = []

    def run(self,points,distance_matrix):

        self.points = points
        self.distance_matrix = distance_matrix

        for point in points:

            if point in self.visited_points:
                continue

            self.visited_points.append(point)
            neighborhood_points = self.region_query(point)

            if len(neighborhood_points) >= self.minimum_cluster_size:
                cluster = []
                self.clusters.append(cluster)
                self.expand_cluster(point, neighborhood_points, cluster)

        return self.clusters

    def region_query(self,base):

        region = []

        for point in self.points:

            if point == base:
                region.append(point)
                continue

            from_to = tuple(sorted([base,point]))
            if self.distance_matrix[from_to] <= self.maximum_intra_cluster_distance:
                region.append(point)

        return region

    def expand_cluster(self, base, neighborhood_points,cluster):

        cluster.append(base)
        self.points_already_in_cluster.append(base)

        for point in neighborhood_points:

            if point not in self.visited_points:
                self.visited_points.append(point)

                subneighborhood_points = self.region_query(point)

                if len(subneighborhood_points) >= self.minimum_cluster_size:
                    neighborhood_points += subneighborhood_points

            if point not in self.points_already_in_cluster:
                cluster.append(point)
                self.points_already_in_cluster.append(point)

if __name__ == '__main__':

    items = [0,1,2,3,4]
    matrix = {(0,1):1, (0,2): 2, (0,3): 13, (0,4): 10, (1,2): 3, (1,3): 10, (1,4): 15, (2,3): 11, (2,4): 12, (3,4): 10}
    clusters = DBSCAN_distance_matrix().run(items,matrix)
