from typing import List, IO
from os import listdir, mkdir
from entity import Entity, split_entity, split_by_space

CSV_FOLDER : str = 'C:\\Users\\wstoop\\Desktop\\scripts\\autoconfig\\data\\entity_csvs\\'
ABBYY_ENTITY_CSV_LOCATION: str = CSV_FOLDER+'as_created_by_abbyy\\'
GOAL_CSV_LOCATION : str = CSV_FOLDER+'split_by_spaces\\'

for folder in listdir(ABBYY_ENTITY_CSV_LOCATION):
    folder += '\\'

    for filename in listdir(ABBYY_ENTITY_CSV_LOCATION+folder):

        new_lines : List[str] = []
        annotation_in_this_document : str = None

        for line in open(ABBYY_ENTITY_CSV_LOCATION+folder+filename):

            try:
                content, top, bottom, left, right, annotation = line.strip().split(';')
                entity: Entity = Entity(int(left), int(right), int(top), int(bottom), content.lower())
                entity.annotation = annotation.strip()
            except ValueError:
                continue

            split_entities : List[Entity] = split_entity(entity,split_by_space)

            for entity in split_entities:
                new_lines.append(';'.join([str(row) for row in [entity.content,entity.top,entity.bottom,entity.left,entity.right,entity.annotation]]))

            if entity.annotation not in ['',None,' ']:
                annotation_in_this_document = entity.annotation

        if annotation_in_this_document == None:
            CSV_SUBLOCATION : str = GOAL_CSV_LOCATION + 'other\\'
        else:
            CSV_SUBLOCATION : str = GOAL_CSV_LOCATION + annotation_in_this_document + '\\'

        try:
            mkdir(CSV_SUBLOCATION)
        except OSError:
            pass

        current_csv: IO = open(CSV_SUBLOCATION + filename + '.csv', 'w')
        current_csv.write('content;top;bottom;left;right;annotations\n')
        current_csv.write('\n'.join(new_lines))