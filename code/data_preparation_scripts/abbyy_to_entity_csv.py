from os import listdir

from code.abbydoc import Abbydoc

ROOT = 'C:/Users/wstoop/Desktop/scripts/autoconfig/'
DATA_FOLDER = ROOT+'data/'
OUTPUT_FOLDER = ROOT+'entity_csvs/potential_fix/'

for folder in listdir(DATA_FOLDER):
    for filename in listdir(DATA_FOLDER+folder):

        current_csv = open(OUTPUT_FOLDER+filename+'.csv','w')

        doc = Abbydoc(DATA_FOLDER+folder+'/'+filename)

        current_csv.write('content;top;bottom;left;right;annotations\n')

        for line in doc.all_lines():
            info_to_show = [line.content, str(line.top), str(line.bottom), str(line.left), str(line.right)]

            try:
                current_csv.write(';'.join(info_to_show))
                current_csv.write(';\n')
            except UnicodeEncodeError:
                continue

        current_csv.close()