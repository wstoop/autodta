import xml.etree.ElementTree as ET
import os

class Abbydoc():

    def __init__(self,filepath):

        #Create basic vars
        self.pages = []

        #Load in the file
        self.path = filepath
        self._load(filepath)

        self.selections = {}

    def _load(self,filepath):

        xmlroot = ET.parse(filepath).getroot()
        pagenr = 0

        for page in xmlroot:

            height = page.get('height')
            width = page.get('width')

            if height == None or width == None:
                continue

            current_page = Page(pagenr,height,width)
            pagenr += 1

            for block in page:

                id = block.get('pageElemId')
                type = block.get('blockType')
                left = block.get('l')
                right = block.get('r')
                top = block.get('t')
                bottom = block.get('b')

                current_block = Block(id,type,top,left,right,bottom)

                for line in self._block_to_lines(block):

                    left = line.get('l')
                    right = line.get('r')
                    top = line.get('t')
                    bottom = line.get('b')

                    current_line = Line(top,left,right,bottom)

                    for formatting in line:

                        for char in formatting:
                            content = char.text
                            left = char.get('l')
                            right = char.get('r')
                            top = char.get('t')
                            bottom = char.get('b')
                            stroke_width = char.get('meanStrokeWidth')
                            confidence = char.get('charConfidence')

                            current_char = Character(content,top,left,right,bottom,stroke_width,confidence)
                            current_line.chars.append(current_char)

                    current_block.lines.append(current_line)

                current_page.blocks.append(current_block)

            self.pages.append(current_page)

    def _block_to_lines(self,block):
        """Helper function to get all lines out of a block"""

        lines = []
        for blockchild in block:
            if blockchild.tag[64:] == 'text':
                for par in blockchild:
                    for line in par:
                        lines.append(line)

            elif blockchild.tag[64:] == 'row':
                for cell in blockchild:
                    for text in cell:
                        for par in text:
                            for line in par:
                                lines.append(line)

        return lines

    def _block_to_chars(self,block):
        """Helper function to get all chars out of a block"""

        chars = []
        for blockchild in block:
            if blockchild.tag[64:] == 'text':
                for par in blockchild:
                    for line in par:
                        for formatting in line:
                            for char in formatting:
                                chars.append(char)

            elif blockchild.tag[64:] == 'row':
                for cell in blockchild:
                    for text in cell:
                        for par in text:
                            for line in par:
                                for formatting in line:
                                    for char in formatting:
                                        chars.append(char)

        return chars

    def all_lines(self):

        all_lines = []

        for page in self.pages:
            for block in page.blocks:
                for line in block.lines:
                    all_lines.append(line)

        return all_lines

    def all_characters(self):

        all_chars = []

        for line in self.all_lines():
            for char in line:
                all_chars.append(char)

        return all_chars

    def get_content(self):

        content = ''
        all_chars = self.all_characters()

        for char in all_chars:
            try:
                print(char.content)
            except UnicodeEncodeError:
                continue

            content += char.content

        return content

    @property
    def mean_character_size(self):

        all_chars = self.all_characters()
        mean_width = sum([char.width for char in all_chars]) / len(all_chars)
        mean_height = sum([char.height for char in all_chars]) / len(all_chars)

        return (mean_width,mean_height)

class Page():

    def __init__(self,nr,height,width):

        self.nr = nr
        self.height = height
        self.width = width
        self.blocks = []

class Block():

    def __init__(self,id,type,top,left,right,bottom):

        self.id = id
        self.type = type
        self.top = int(top)
        self.bottom = int(bottom)
        self.left = int(left)
        self.right = int(right)

        self.pos = (self.top,self.left)
        self.size = (self.right-self.left,self.bottom-self.top)

        self.lines = []

class Line():

    def __init__(self,top,left,right,bottom):

        self.id = id
        self.type = type
        self.top = int(top)
        self.bottom = int(bottom)
        self.left = int(left)
        self.right = int(right)

        self.pos = (self.top,self.left)
        self.size = (self.right-self.left,self.bottom-self.top)

        self.chars = []

    @property
    def content(self):
        return ''.join([char.content for char in self.chars])


class Character():

    def __init__(self,content,top,left,right,bottom,stroke_width,confidence):

        self.content = content

        self.top = int(top)
        self.bottom = int(bottom)
        self.left = int(left)
        self.right = int(right)

        self.pos = (self.left,self.top)
        self.width = self.right-self.left
        self.height = self.bottom-self.top
        self.size = (self.width,self.height)

        try:
            self.stroke_width = int(stroke_width)
        except TypeError: #Sometimes there is not stroke width (spaces for example)
            self.stroke_width = 0

        try:
            self.confidence = int(confidence)
        except TypeError:
            self.confidence = 0

if __name__ == '__main__':

    import viewer

    folder = 'example_docs/'
    docs = []

    for filename in os.listdir(folder):

        if '.xml' not in filename:
            continue

        docs.append(Abbydoc(folder+filename))

    viewer.DocViewer(docs)

#Suggestion prediction bijwerken
#Direction is nog hard coded