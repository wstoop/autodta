from enum import Enum
from typing import List, Dict

from document import Document
from entity import Entity
from cluster_labels import Cluster, cluster_terms

class Direction(Enum):
    top = 'top'
    bottom = 'bottom'
    left = 'left'
    right = 'right'

def entity_is_in_column_of_other_entity(entity : Entity,column_defining_entity : Entity) -> bool:

    if (entity.left >= column_defining_entity.left and entity.right <= column_defining_entity.right) or \
        (entity.left <= column_defining_entity.left and entity.right >= column_defining_entity.left) or \
        (entity.left <= column_defining_entity.right and entity.right >= column_defining_entity.right ):
        return True

    else:
        return False

def entity_is_in_row_of_other_entity(entity : Entity, row_defining_entity : Entity) -> bool:
    if (entity.top >= row_defining_entity.top and entity.bottom <= row_defining_entity.bottom) or \
            (entity.top <= row_defining_entity.top and entity.bottom >= row_defining_entity.top) or \
            (entity.top <= row_defining_entity.bottom and entity.bottom >= row_defining_entity.bottom):
        return True

    else:
        return False

def get_closest_labels_in_all_directions(all_entities : List[Entity],interesting_entity : Entity) -> Dict[Direction,str]:

    ALL_DIRECTIONS : List[Direction] = [Direction.bottom,Direction.top,Direction.right,Direction.left]

    distance_for_potential_labels: Dict[Direction, Dict[Entity, float]] = {Direction.top: {}, Direction.bottom: {},
                                                                           Direction.left: {},
                                                                           Direction.right: {}}

    closest_labels : Dict[Direction, str] = {}
    for entity in all_entities:

        if entity == interesting_entity:
            continue

        # Vertical
        if entity_is_in_column_of_other_entity(entity, interesting_entity):
            distance = interesting_entity.center[1] - entity.center[1]

            if distance > 0:
                distance_for_potential_labels[Direction.top][entity] = distance
            elif distance < 0:
                distance_for_potential_labels[Direction.bottom][entity] = -distance

        # Horizontal
        elif entity_is_in_row_of_other_entity(entity, interesting_entity):
            distance = interesting_entity.center[0] - entity.center[0]

            if distance > 0:
                distance_for_potential_labels[Direction.left][entity] = distance
            elif distance < 0:
                distance_for_potential_labels[Direction.right][entity] = -distance

    for direction in ALL_DIRECTIONS:

        if len(distance_for_potential_labels[direction].items()) < 1:
            continue

        closest_entity: Entity = sorted(distance_for_potential_labels[direction].items(), key=lambda x: x[1])[0][0]
        closest_labels[direction] = closest_entity.content

    return closest_labels

def get_all_labels(documents : List[Document], annotations_to_filter : List[str]) -> Dict[Direction, List[Cluster]]:

    ALL_DIRECTIONS : List[Direction] = [Direction.bottom,Direction.top,Direction.right,Direction.left]
    closest_labels : Dict[Direction, List[str]] = {Direction.top: [], Direction.bottom: [], Direction.left: [],
                                         Direction.right: []}
    clusters : Dict[Direction, List[Cluster]] = {Direction.top: [], Direction.bottom: [], Direction.left: [],
                                         Direction.right: []}
    for doc in documents:

        #Check in each direction which other label is closest to the label of interest
        interesting_entity : Entity

        for entity in doc.entities:
            if entity.annotation in annotations_to_filter:
                interesting_entity = entity
                break

        closest_labels_for_this_doc : Dict[Direction,str] = get_closest_labels_in_all_directions(doc.entities,interesting_entity)

        for direction, label in closest_labels_for_this_doc.items():
            closest_labels[direction].append(label)

    #Now cluster the findings for each of the directions
    for direction in ALL_DIRECTIONS:
        clusters[direction] = sorted([Cluster(cluster) for cluster in cluster_terms(closest_labels[direction])],key=lambda x: x.size, reverse=True)

    return clusters
