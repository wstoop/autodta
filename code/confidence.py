from typing import List, Tuple, Dict
from document import Document
from collections import Counter

def decide_class_based_on_confidence(confidences : List[List[float]],boundary : float) -> List[int]:

    result : List[int] = []

    for classA, classB in confidences:

        if classB >= boundary:
            result.append(1)
        else:
            result.append(0)

    return result


def decide_class_based_on_source_document(confidences : List[List[float]],docs : List[Document]) -> List[int]:

    total_entity_index : int = 0
    total_results : List[int] = []

    for doc in docs:

        highest_confidence : float = 0
        index_of_most_likely_entity : int = None
        results_for_this_doc : List[int] = []

        for local_entity_index in range(len(doc.entities)):

            confidence_this_is_entity_in_focus : float = confidences[total_entity_index][1]

            results_for_this_doc.append(0)

            if confidence_this_is_entity_in_focus > highest_confidence or index_of_most_likely_entity == None:
                highest_confidence = confidence_this_is_entity_in_focus
                index_of_most_likely_entity = local_entity_index

            total_entity_index += 1

        results_for_this_doc[index_of_most_likely_entity] = 1
        total_results+= results_for_this_doc

    return total_results


def correct_answer_is_in_n_most_confident_entities(confidences : List[List[float]],docs : List[Document],target : List[int],n : int):

    total_entity_index : int = 0

    rank_of_correct_entity_per_document : List[int] = []
    success_per_document : List[int] = []

    for doc in docs:

        confidences_for_this_document : List[Tuple[int,float]] = []
        correct_entity_index = None

        for local_entity_index in range(len(doc.entities)):

            confidences_for_this_document.append((local_entity_index,confidences[total_entity_index][1]))
            target_for_this_entity = target[total_entity_index]

            if target_for_this_entity:
                correct_entity_index = local_entity_index

            total_entity_index += 1

        confidences_for_this_document = sorted(confidences_for_this_document,key=lambda x: x[1],reverse=True)
        most_likely_indices = [index for index,confidence in confidences_for_this_document[:n]]

        try:
            rank_of_correct_entity_per_document.append(most_likely_indices.index(correct_entity_index))
        except ValueError:
            rank_of_correct_entity_per_document.append(-1)

        if correct_entity_index in most_likely_indices:
            success_per_document.append(1)
        else:
            success_per_document.append(0)

    print(rank_of_correct_entity_per_document)

    return success_per_document

def pick_entity_per_document_by_normalized_confidence(confidences_a : List[float], confidences_b : List[float],
                                                      documents: List[Document]) -> List[int]:

    total_entity_index : int = 0
    results : List[int] = []

    for doc in documents:

        confidence_a_for_this_document : List[float] = []
        confidence_b_for_this_document : List[float] = []

        for local_entity_index in range(len(doc.entities)):

            confidence_a_for_this_document.append(confidences_a[total_entity_index])
            confidence_b_for_this_document.append(confidences_b[total_entity_index])

            total_entity_index += 1

        summed_confidence_per_local_entity_index : Dict[int,float] = {}

        for n, (confidence_a, confidence_b) in enumerate(zip(confidence_a_for_this_document,confidence_b_for_this_document)):

            summed_confidence_per_local_entity_index[n] = confidence_a+confidence_b

        sorted_summed_confidences : List[Tuple[int, float]] = sorted(summed_confidence_per_local_entity_index.items(), key=lambda x: x[1],reverse=True)
        index_of_most_likely_entity = sorted_summed_confidences[0][0]

        for local_entity_index in range(len(doc.entities)):
            if local_entity_index == index_of_most_likely_entity:
                results.append(1)
            else:
                results.append(0)

    return results


def pick_entity_per_document_by_rank(confidences_a : List[List[float]], confidences_b : List[List[float]],
                                                                documents : List[Document]) -> List[int]:

    total_entity_index : int = 0
    results : List[int] = []

    for doc in documents:

        confidence_a_for_this_document : List[Tuple[int,float]] = []
        confidence_b_for_this_document : List[Tuple[int,float]] = []

        for local_entity_index in range(len(doc.entities)):

            confidence_a_for_this_document.append((local_entity_index,confidences_a[total_entity_index][1]))
            confidence_b_for_this_document.append((local_entity_index,confidences_b[total_entity_index][1]))

            total_entity_index += 1

        confidence_a_for_this_document = sorted(confidence_a_for_this_document,key=lambda x: x[1],reverse=True)
        confidence_b_for_this_document = sorted(confidence_b_for_this_document,key=lambda x: x[1],reverse=True)

        summed_ranking_per_local_entity_index : Dict[int,int] = {}

        rank : int = -1
        last_confidence : float = None
        for index, confidence in confidence_a_for_this_document:

            if last_confidence != confidence:
                rank += 1
                last_confidence = confidence

            summed_ranking_per_local_entity_index[index] = rank
            rank += 1

        rank = -1
        last_confidence = None

        for index, confidence in confidence_b_for_this_document:

            if last_confidence != confidence:
                rank += 1
                last_confidence = confidence

            summed_ranking_per_local_entity_index[index] += rank
            rank += 1

        sorted_summed_rankings : List[Tuple[int, int]] = sorted(summed_ranking_per_local_entity_index.items(), key=lambda x: x[1])
        index_of_most_likely_entity = sorted_summed_rankings[0][0]

        for local_entity_index in range(len(doc.entities)):
            if local_entity_index == index_of_most_likely_entity:
                results.append(1)
            else:
                results.append(0)

    return results

def list_of_confidences_to_list_of_ranks_per_document(confidences : List[List[float]], documents : List[Document]) -> List[int]:

    total_rankings : List[int] = []
    total_entity_index : int = 0

    for doc in documents:

        confidences_for_this_document : List[Tuple[int,float]] = []

        for local_entity_index in range(len(doc.entities)):

            confidences_for_this_document.append((local_entity_index,confidences[total_entity_index][1]))
            total_entity_index += 1

        confidences_for_this_document = sorted(confidences_for_this_document,key=lambda x: x[1],reverse=True)

        rank : int = -1
        last_confidence : float = None
        rankings_per_entity_index : Dict[int,int] = {}

        for index, confidence in confidences_for_this_document:
            if last_confidence != confidence:
                rank += 1
                last_confidence = confidence

            rankings_per_entity_index[index] = rank

        for local_entity_index in range(len(doc.entities)):
            total_rankings.append(rankings_per_entity_index[local_entity_index])

    return total_rankings

def normalize_confidences_per_document(confidences : List[List[float]], documents : List[Document]) -> List[float]:

    total_normalized_confidences : List[float] = []
    total_entity_index : int = 0

    for doc in documents:

        confidences_for_this_document : List[Tuple[int,float]] = []

        for local_entity_index in range(len(doc.entities)):

            confidences_for_this_document.append((local_entity_index,confidences[total_entity_index][1]))
            total_entity_index += 1

        confidences_for_this_document = sorted(confidences_for_this_document,key=lambda x: x[1],reverse=True)

        #First figure out the ranks
        rank : int = -1
        last_confidence : float = None
        rankings_per_entity_index : Dict[int,int] = {}

        for index, confidence in confidences_for_this_document:
            if last_confidence != confidence:
                rank += 1
                last_confidence = confidence

            rankings_per_entity_index[index] = rank

        #Translate the ranking to a normalized confidence
        all_rankings : List[int] = [i[1] for i in rankings_per_entity_index.items()]
        ranking_frequencies : List[Tuple[int,int]] = list(Counter(all_rankings).items())
        ordered_ranking_frequencies = sorted(ranking_frequencies, key=lambda x: x[0])
        nr_items_per_rank : List[int] = [nr for rank,nr in ordered_ranking_frequencies]

        normalized_confidences_per_entity_index : Dict[int,float] = {}

        for index, rank in rankings_per_entity_index.items():
            normalized_confidences_per_entity_index[index] = sum(nr_items_per_rank[rank+1:])/sum(nr_items_per_rank)

        for local_entity_index in range(len(doc.entities)):
            total_normalized_confidences.append(normalized_confidences_per_entity_index[local_entity_index])

    return total_normalized_confidences
