from typing import List
from entity import Entity

from os import listdir

class Document():

    def __init__(self,name : str) -> None:

        self.name : str = name
        self.entities : List[Entity] = []

    def contains_annotation(self,annotations : List[str]) -> bool:

        for entity in self.entities:
            for annotation in annotations:
                if entity.annotation == annotation:
                    return True

        return False

def load_documents(data_root : str, folders_to_use : List[str]) -> List[Document]:

    all_docs : List[Document] = []

    for folder in folders_to_use:
        for filename in listdir(data_root + folder):

            current_document = Document(filename)

            for line in open(data_root + folder + '\\' + filename):

                try:
                    content, top, bottom, left, right, annotation = line.strip().split(';')
                    entity : Entity = Entity(int(left), int(right), int(top), int(bottom), content.lower())
                    entity.annotation = annotation.strip()
                except ValueError:
                    continue

                current_document.entities.append(entity)

            all_docs.append(current_document)

    return all_docs

def divide_into_folds(documents : List[Document],nr_of_folds : int) -> List[List[Document]]:

    group_size : int = round(len(documents) / nr_of_folds)
    folds : List[List[Document]] = []

    start_doc_index : int
    end_doc_index : int

    for fold_index in range(nr_of_folds):

        start_doc_index = fold_index*group_size
        end_doc_index = start_doc_index + group_size

        if end_doc_index > len(documents):
            end_doc_index = len(documents) -1

        folds.append(documents[start_doc_index:end_doc_index])

    if end_doc_index <= len(documents) -1:
        folds[-1] += documents[end_doc_index:]

    return folds