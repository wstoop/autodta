from os import listdir

def import_expected_values_for_field(folder,field):

    expected_values = []
    FIELD_NAME_COLUMN_INDEX = 1
    TEXT_VALUE_COLUMN_INDEX = 2

    for filename in listdir(folder):
        for line in open(folder+filename,encoding='utf-8'):
            columns = line.split(';')

            if columns[FIELD_NAME_COLUMN_INDEX] == field:
                value = columns[TEXT_VALUE_COLUMN_INDEX]

                if value != '':
                    expected_values.append(value)

    return expected_values


def normalize_date_format(date):

    if len(date) < 1:
        return False

    if date[0] == '0':
        date = date[1:]

    if date[-4:-2] in ['19','20']:
        date = date[:-4] + date[-2:]

    return date.replace('-0','-').replace('/0','/').replace('/','||').replace('-','||')


def remove_separators_from_date(date):

    return date.replace('/','').replace('-','')


def annotate_line(filename,line_index,annotation):

    ANNOTATION_COLUMN_INDEX = 5
    all_lines = open(filename).readlines()

    columns_to_extend = all_lines[line_index].split(';')
    try:
        columns_to_extend[ANNOTATION_COLUMN_INDEX] = annotation
    except IndexError:
        print('Annotation failed for file',filename)

    all_lines[line_index] = ';'.join(columns_to_extend)+'\n'

    open(filename,'w').write(''.join(all_lines))

if __name__ == '__main__':

    ROOT = 'C:/Users/wstoop/Desktop/scripts/autoconfig/'
    EXPECTED_VALUES_FOLDER = ROOT+'expected_values/'
    ENTITY_CSV_FOLDER = ROOT+'entity_csvs/'
    FIELD = 'GeboorteDt'

    expected_values = import_expected_values_for_field(EXPECTED_VALUES_FOLDER,FIELD)

    for file in listdir(ENTITY_CSV_FOLDER):

        filepath = ENTITY_CSV_FOLDER+file
        matches_to_expected_values = 0

        for n,line in enumerate(open(filepath)):
            columns = line.split(';')
            value_to_match = columns[0]

            for expected_value in expected_values:

                if normalize_date_format(value_to_match) == normalize_date_format(expected_value):
                    matches_to_expected_values += 1
                    annotate_line(filepath,n,'GD')
                    break
                elif remove_separators_from_date(value_to_match) == remove_separators_from_date(expected_value):
                    matches_to_expected_values += 1
                    annotate_line(filepath,n,'GDS')
                    break
                elif normalize_date_format(value_to_match) == 'Geboortedatum '+normalize_date_format(expected_value):
                    matches_to_expected_values += 1
                    annotate_line(filepath,n,'GD')
                    break

        if matches_to_expected_values == 0:
            print('nothing for',file)
        elif matches_to_expected_values > 1:
            print(file)

#Check doubles