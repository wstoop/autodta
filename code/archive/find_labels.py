from collections import Counter, defaultdict
from enum import Enum
from os import listdir

from code.cluster_labels import cluster_terms, cluster_terms_multiple_layers


class Direction(Enum):

    top = 'top'
    bottom = 'bottom'
    left = 'left'
    right = 'right'

class Entity():

    def __init__(self,left,right,top,bottom,content):
        self.left = int(left)
        self.right = int(right)
        self.top = int(top)
        self.bottom = int(bottom)
        self.content = content

    @property
    def center(self):
        return self.left+(self.right-self.left)*0.5, self.bottom+(self.bottom-self.top)*0.5

def entity_is_in_column_of_other_entity(entity,column_defining_entity):

    if (entity.left >= column_defining_entity.left and entity.right <= column_defining_entity.right) or \
        (entity.left <= column_defining_entity.left and entity.right >= column_defining_entity.left) or \
        (entity.left <= column_defining_entity.right and entity.right >= column_defining_entity.right ):
        return True

    else:
        return False


def entity_is_in_row_of_other_entity(entity, row_defining_entity):
    if (entity.top >= row_defining_entity.top and entity.bottom <= row_defining_entity.bottom) or \
            (entity.top <= row_defining_entity.top and entity.bottom >= row_defining_entity.top) or \
            (entity.top <= row_defining_entity.bottom and entity.bottom >= row_defining_entity.bottom):
        return True

    else:
        return False

def find_potential_labels_for_annotated_entity(root_folder,folders_to_use,labels_to_filter,nr_of_closest_labels=1):

    potential_labels = {Direction.top: [], Direction.bottom: [], Direction.left: [], Direction.right: []}

    for folder in folders_to_use:
        for filename in listdir(root_folder + folder):

            entity_of_interest = None
            other_entities_in_this_document = []
            distance_for_potential_labels = {Direction.top: {}, Direction.bottom: {}, Direction.left: {}, Direction.right: {}}

            for line in open(root_folder + folder + '\\' + filename):

                try:
                    content, top, bottom, left, right, annotation = line.strip().split(';')
                    entity = Entity(left, right, top, bottom, content.lower())

                    if annotation in labels_to_filter:
                        entity_of_interest = entity
                    else:
                        other_entities_in_this_document.append(entity)

                except ValueError:
                    continue

            if entity_of_interest == None:
                continue

            for other_entity in other_entities_in_this_document:

                #Vertical
                if entity_is_in_column_of_other_entity(other_entity, entity_of_interest):
                    distance = entity_of_interest.center[1] - other_entity.center[1]

                    if distance > 0:
                        distance_for_potential_labels[Direction.top][other_entity] = distance
                    elif distance < 0:
                        distance_for_potential_labels[Direction.bottom][other_entity] = distance

               #Horizontal
                elif entity_is_in_row_of_other_entity(other_entity, entity_of_interest):
                    distance = entity_of_interest.center[0] - other_entity.center[0]

                    if distance > 0:
                        distance_for_potential_labels[Direction.left][other_entity] = distance
                    elif distance < 0:
                        distance_for_potential_labels[Direction.right][other_entity] = distance

            for direction in [Direction.bottom,Direction.top,Direction.right,Direction.left]:
                potential_labels[direction] += sorted(distance_for_potential_labels[direction].items(), key=lambda x: x[1])[:nr_of_closest_labels]

    return potential_labels

def flip_nested_dictionary(d):

    flipped = defaultdict(dict)
    for key, val in d.items():
        for subkey, subval in val.items():
            flipped[subkey][key] = subval

    return flipped

def create_frequency_matrix_per_term(dta_dicts_per_nr_of_closest_labels,ignore_infrequent_labels=True):

    frequencies_per_label = flip_nested_dictionary(dta_dicts_per_nr_of_closest_labels)

    if ignore_infrequent_labels:
        INFREQUENCY_THRESHOLD = 150
        frequencies_per_frequent_label = {}

        for label, frequencies in frequencies_per_label.items():
            summed_frequency = sum(frequencies.values())

            if summed_frequency > INFREQUENCY_THRESHOLD:
                frequencies_per_frequent_label[label] = frequencies

        frequencies_per_label = frequencies_per_frequent_label

    x_ticks = sorted(dta_dicts_per_nr_of_closest_labels.keys())
    y_ticks = sorted(frequencies_per_label.keys())

    print('\t'.join(['']+y_ticks))

    for x_tick in x_ticks:
        r = str(x_tick)+'\t'

        for y_tick in y_ticks:

            try:
                r += str(frequencies_per_label[y_tick][x_tick])+'\t'
            except KeyError:
                r += '0\t'

        print(r)

if __name__ == '__main__':
    ROOT_FOLDER = 'C:\\Users\\wstoop\\Desktop\\scripts\\autoconfig\\entity_csvs\\automated_annotation\\'
    FOLDERS_TO_USE = ['auto_annotated','done_manual']
    LABELS_TO_FILTER = ['GD']
    NR_OF_CLOSEST_LABELS = 1
    MULTIPLE_CLUSTER_LAYERS = True

    dta_dicts_per_nr_of_closest_labels = {}

    potential_labels = find_potential_labels_for_annotated_entity(ROOT_FOLDER,FOLDERS_TO_USE,LABELS_TO_FILTER,NR_OF_CLOSEST_LABELS)

    for direction, dictionary in potential_labels.items():
        print(direction)
        raw_dta_dictionary = [label.content for label, distance in dictionary]

        if MULTIPLE_CLUSTER_LAYERS:
            cluster_terms_multiple_layers(raw_dta_dictionary)

        else:
            dta_dictionary = dict(Counter(raw_dta_dictionary).most_common(50))

            #First show clusters
            clusters = cluster_terms(list(dta_dictionary.keys()))
            all_clustered_terms = []
            for cluster in clusters:
                frequency = sum([dta_dictionary[term] for term in cluster])
                print(cluster,frequency)

                all_clustered_terms += cluster

            #Show all terms not in cluster
            for term,freq in dta_dictionary.items():

                if freq > 1 and term not in all_clustered_terms:
                    print(term,freq)

            print()