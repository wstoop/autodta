#This script fixes a bug in the positions of the entities, discovered after annotation

from os import listdir
from os.path import isfile

from code.abbydoc import Abbydoc

ROOT = 'C:/Users/wstoop/Desktop/scripts/autoconfig/'
DATA_FOLDER = ROOT+'data/'
OUTPUT_FOLDER_ROOT = ROOT+'entity_csvs/automated_annotation/'

for folder in listdir(DATA_FOLDER):
    for filename in listdir(DATA_FOLDER+folder):

        #First find the corresponding outputfile for each input abbyy xml
        found_corresponding_output_file = False

        for outputfolder in listdir(OUTPUT_FOLDER_ROOT):

            outputfile_path = OUTPUT_FOLDER_ROOT+outputfolder+'/'+filename+'.csv'

            if isfile(outputfile_path):
                found_corresponding_output_file = True
                break

        if not found_corresponding_output_file:
            print('nothing found for',filename)
            continue

        #Find out which annotation is done on which line
        annotations_by_line_index = {}

        for n,line in enumerate(open(outputfile_path)):
            columns = line.strip().split(';')

            if columns[-1] in ['GD','GDS','GD0.5','GD1','GD@']:
                annotations_by_line_index[n-1] = columns[-1]
                break

        doc = Abbydoc(DATA_FOLDER+folder+'/'+filename)

        current_csv = open(outputfile_path,'w')
        current_csv.write('content;top;bottom;left;right;annotations\n')

        for n,line in enumerate(doc.all_lines()):
            info_to_show = [line.content, str(line.top), str(line.bottom), str(line.left), str(line.right)]

            try:
                current_csv.write(';'.join(info_to_show))

                if n in annotations_by_line_index.keys():
                    current_csv.write(';'+annotations_by_line_index[n]+'\n')
                else:
                    current_csv.write(';\n')

            except UnicodeEncodeError:
                continue

        current_csv.close()