#This is a script to verify that for a number of document, the cross entity selection works correclty

from typing import List, Dict

from code.run_experiment import Document, Entity, Direction, Cluster, get_closest_labels_in_all_directions, \
    load_documents, get_all_labels

DATA_ROOT: str = 'C:\\Users\\wstoop\\Desktop\\scripts\\autoconfig\\entity_csvs\\automated_annotation\\'
FOLDERS_TO_USE: List[str] = ['auto_annotated', 'done_manual']
ANNOTATIONS_TO_FILTER: List[str] = ['GD']

DOCUMENTS_OF_INTEREST : List[str] = ['003b71d22177ac93f6e74fdd62548607e16159e2348568e435b4f50c6fcffd56_3',
                                        '043a1cde9ba332452149ecf0459496da644c25e1c1b64ded45f4eeff9f849349_3',
                                        '19dd7973e13f04bd12659089df7fdfac123608720cca77a3fb4202495b6c49c1_3',
                                        '1ac1a4906601217e525f0e1af8064d4ee7a7b7db65485020dd914146e1d197b7_3',
                                        '39067bfbe89c81ac14b1a8c51896eb10afdd1bc86d4d5692195fbe8670a79e9e_3',
                                        '403db0b1dfda43e00676aac908fddadfdc4b52ce22e86d6aa14111eead6d0af8_3',
                                        '4c6170670f661865ea3b56514e531e844752ce72c661d661d7529e9f9eff1e1b_3',
                                        '571480ec3f2a12b1961dd93bc44a8aa2041efc754c294b2e82af08e2563ac7dc_3',
                                        '8a847129554f9ed42834e86e279f2fc054d0e6e2376294d6de7fc0e3189ccae6_3',
                                        'a823436c919eb4168e1ab5218650e11986e507fd1f775520f9bb738eefb81f2f_3',
                                        'b4d4b55a879036708692d20d440203621325543d2776ca6838bff3c0a55ae842_3',
                                        'bc600ee4631fb1060b1db27d9755139abf30765f70f600d4c02d92b0949be30c_3',
                                        'f90defe015a73ca22012845bb3fc846923bf82f39daac826ea78946f3c11fd8c_3',
                                        '7a9407fb480731a56d7e45648c971ab30465f84c7c6a92adc60f4433671b3569_3',
                                        '859da5ed8179cefb08adef7341a1874182b88455f23627c8d51b10e72864bfe2_3']

all_documents: List[Document] = load_documents(DATA_ROOT, FOLDERS_TO_USE)
all_labels : Dict[Direction, List[Cluster]] = get_all_labels(all_documents,ANNOTATIONS_TO_FILTER)

for dir, clusters in all_labels.items():
    print(dir)
    for cluster in clusters:
        print(cluster.items,cluster.is_part_of_cluster('functiegr. geb. datum'))

    print()

for doc in all_documents:
    for interesting_doc_name in DOCUMENTS_OF_INTEREST:
        if interesting_doc_name in doc.name:

            for entity in doc.entities:
                if entity.annotation in ANNOTATIONS_TO_FILTER:
                    interesting_entity : Entity = entity
                    break

            closest_labels : Dict[Direction,str] = get_closest_labels_in_all_directions(doc.entities,interesting_entity)
            print(interesting_doc_name,closest_labels)