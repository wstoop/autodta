from typing import List, Callable, Tuple, Dict
from collections import Counter
from itertools import combinations
from cluster_algorithms.dbscan import DBSCAN_distance_matrix  # type: ignore

class Cluster():

    def __init__(self, items : List[str]) -> None:
        self.items : List[str] = items
        self.unique_items_by_freq : List[str] = [i[0] for i in sorted(list(Counter(self.items).items()),key=lambda x: x[1],reverse=True)]
        self.max_neighbor_distance : float = self.get_interitem_distance()

    @property
    def example(self) -> str: #Returns the most common item
        return max(set(self.items), key=self.items.count)

    @property
    def size(self) -> int:
        return len(self.items)

    def get_interitem_distance(self) -> float:
        """Get the maximum distance between two nearest neighbors"""

        smallest_distances_per_entity : List[float] = []

        for n, item in enumerate(self.items):
            smallest_distance : float = None

            for o, subitem in enumerate(self.items):
                if n == o: #We cannot compare the items themselves, because they might be identical but separate items
                    continue

                distance : float = weighted_levenshtein(item,subitem)

                if smallest_distance == None or distance < smallest_distance:
                    smallest_distance = distance

            smallest_distances_per_entity.append(smallest_distance)

        return max(smallest_distances_per_entity)

    def is_part_of_cluster(self,s : str) -> bool:

        if s in self.items:
            return True
        else:
            for item in self.unique_items_by_freq:
                if weighted_levenshtein(s,item) < self.max_neighbor_distance:
                    return True

        return False

    def __repr__(self) -> str:
        return '<Cluster: '+self.example+' ('+str(self.size)+')>'


def create_distance_matrix(items : List[str],distance_function : Callable) -> Dict[Tuple[int,int],float]:

    matrix : Dict[Tuple[int,int],float] = {}
    item_indices : List[int] = list(range(len(items)))

    for i1,i2 in combinations(item_indices,2):
        matrix[(i1,i2)] = distance_function(items[i1],items[i2]) / max([len(items[i1]), len(items[i2])])

    return matrix

def levenshtein(s1 : str, s2 : str) -> int:

    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = list(range(len(s2) + 1))

    for i, c1 in enumerate(s1):
        current_row = [i + 1]

        for j, c2 in enumerate(s2):

            insertions = previous_row[
                             j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]

def weighted_levenshtein(s1 : str,s2 : str) -> float:

    return levenshtein(s1, s2) / max([len(s1), len(s2)])

def cluster_terms(terms : List[str], algo : Callable=DBSCAN_distance_matrix) -> List[List[str]]:

    matrix : Dict[Tuple[int,int],float] = create_distance_matrix(terms,levenshtein)
    indices : List[int] = list(range(len(terms)))

    clusters : List[List[int]] = algo().run(indices,matrix)

    return index_clusters_to_term_clusters(clusters,terms)

def cluster_terms_multiple_layers(terms): #Note: untyped

    STEP_SIZE = 0.1

    matrix = create_distance_matrix(terms,levenshtein)
    indices = list(range(len(terms)))
    current_maximum_intra_cluster_distance = 1

    previous_clusters = [indices]

    while current_maximum_intra_cluster_distance >= 0:

        current_clusters = []

        for previous_cluster in previous_clusters:
            algo = DBSCAN_distance_matrix()
            algo.maximum_intra_cluster_distance = current_maximum_intra_cluster_distance

            clusters = algo.run(previous_cluster,matrix)

            current_maximum_intra_cluster_distance -= STEP_SIZE
            current_clusters += clusters

        print(index_clusters_to_term_clusters(current_clusters,terms))
        previous_clusters = current_clusters

    print()

def index_clusters_to_term_clusters(index_clusters : List[List[int]],terms : List[str]) -> List[List[str]]:

    result : List[List[str]] = []

    for cluster in index_clusters:
        result.append([terms[i] for i in cluster])

    return result

if __name__ == '__main__':
    terms : List[str] = ['wessel','wesse1','wess3l','wessell','wesssel','st00p','stoop','is','cool','c00l']*3
    r : List[List[str]] = cluster_terms(terms[:23],algo=DBSCAN_distance_matrix)
    print(r)