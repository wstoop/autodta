from typing import Tuple, List, Callable

class Entity():

    def __init__(self,left : int,right : int,top : int,bottom : int,content : str) -> None:
        self.left : int = left
        self.right : int = right
        self.top : int = top
        self.bottom : int = bottom
        self.content : str = content
        self.annotation : str = None

    @property
    def center(self) -> Tuple[float,float]:
        return self.left+(self.right-self.left)*0.5, self.bottom+(self.bottom-self.top)*0.5

    def __repr__(self) -> str:
        return '<Entity: '+str(self.content)+' ('+str(self.left)+','+str(self.right)+','+str(self.top)+','+str(self.bottom)+'>'

def split_entity(original_entity : Entity,split_function : Callable) -> List[Entity]:

    entity_length : int = original_entity.right - original_entity.left
    split_entities : List[Entity] = []
    left_offset : int = 0

    parts : List[str] = split_function(original_entity.content)
    total_length : int = sum([len(part) for part in parts])

    for part in parts:
        length_percentage : float = len(part) / total_length
        length_of_split_entity : int = round(length_percentage*entity_length)
        new_entity = Entity(original_entity.left+left_offset,original_entity.left+left_offset+length_of_split_entity,
                            original_entity.top,original_entity.bottom,part)
        new_entity.annotation = original_entity.annotation

        split_entities.append(new_entity)
        left_offset += length_of_split_entity

    return split_entities

def split_by_space(s : str):
    return s.split()