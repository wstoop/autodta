from collections import Counter
from functools import partial
from multiprocessing import Pool
from os import mkdir
from pickle import dump, load
from shutil import rmtree
from typing import List, Dict, Any

from numpy import logspace  # type: ignore
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import recall_score, precision_score, f1_score  # type: ignore
from sklearn.model_selection import GridSearchCV  # type: ignore

from sklearn.neighbors import KNeighborsClassifier  # type: ignore
from sklearn.svm import SVC # type : ignore
from sklearn.neural_network import MLPClassifier #type : ignore
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier

from document import Document, load_documents, divide_into_folds
from cluster_labels import Cluster
from find_labels import Direction, get_closest_labels_in_all_directions, get_all_labels
from confidence import decide_class_based_on_source_document, pick_entity_per_document_by_rank, pick_entity_per_document_by_normalized_confidence, \
                        list_of_confidences_to_list_of_ranks_per_document, normalize_confidences_per_document

def create_environment_instances_based_on_labels(document : Document, label_clusters : Dict[Direction, List[Cluster]], annotations_of_interest : List[str]):

    ALL_DIRECTIONS : List[Direction] = [Direction.bottom,Direction.top,Direction.right,Direction.left]

    target : List[int] = []
    instances : List[List[bool]] = []

    for entity in document.entities:
        features : List[bool] = []
        closest_labels : Dict[Direction,str] = get_closest_labels_in_all_directions(document.entities,entity)

        for direction in ALL_DIRECTIONS:
            label_clusters_in_direction = label_clusters[direction]

            closest_label_in_direction : str

            try:
                closest_label_in_direction = closest_labels[direction]
            except KeyError:
                closest_label_in_direction = None

            for label_cluster in label_clusters_in_direction:

                if closest_label_in_direction == None:
                    features.append(False)
                else:
                    features.append(label_cluster.is_part_of_cluster(closest_label_in_direction))

        instances.append(features)
        target.append(int(entity.annotation in annotations_of_interest))

    return target, instances

def create_content_instances(document : Document, annotations_of_interest : List[str]):

    MAX_LENGTH = 10

    target : List[int] = []
    instances : List[List[int]] = []

    for entity in document.entities:

        content = entity.content[:MAX_LENGTH]

        while len(content) < MAX_LENGTH:
            content += '_'

        instances.append([ord(character) for character in content])
        target.append(int(entity.annotation in annotations_of_interest))

    return target, instances

def traintest(classifier,run_name : str, test_docs : List[Document], training_instances : List[List[bool]],
              test_instances : List[List[bool]],training_target : List[int], test_target : List[int]):

    print('* Train the '+run_name+' model')
    classifier.fit(training_instances, training_target)

    print('* Test the '+run_name+' model')
    confidences: List[List[float]] = list(classifier.predict_proba(test_instances))

    results: List[int] = decide_class_based_on_source_document(confidences,test_docs)

    print('  * p', precision_score(test_target, results),
          'r', recall_score(test_target, results),
          'f', f1_score(test_target, results))

    return results, confidences

def show_errors(results : List[int], confidences_a : List[float], confidences_b : List[float],
                target: List[int], entity_contents : List[str]):

    INDENT_SIZE = 6
    indent = INDENT_SIZE*' '
    log : str

    confidences_summed = [sum([a,b]) for a,b in zip(confidences_a,confidences_b)]

    things_to_log : List[List[Any]] = [entity_contents, confidences_a, confidences_b,confidences_summed]

    for n, result in enumerate(results):

        if result == 0 and target[n] == 1:
            log = indent+'False negative'

            for i in things_to_log:

                try:
                    log += ' '+str(i[n])
                except IndexError:
                    pass

            print(log)

        elif result == 1 and target[n] == 0:

            log = indent+'False positive'

            for i in things_to_log:

                try:
                    log += ' '+str(i[n])
                except IndexError:
                    pass

            print(log)

def run_experiment(documents : List[Document], nr_of_folds : int, annotations_to_filter : List[str], use_cache : bool,
                   cache_location : str, nr_of_threads : int) -> None:

    #These are experimental settings, will make it to the main script once successful
    USE_TUNING : bool = False
    PARAM_GRID = [{'C': logspace(-2,10,5), 'gamma':logspace(-9,3,5)}]
    ERROR_ANALYSIS = True

    #Clear the cache
    if not use_cache:
        print('* Clean the instance cache')
        try:
            rmtree(cache_location)
        except FileNotFoundError:
            pass

        mkdir(cache_location)

    #Start the experiment
    folds : List[List[Document]] = divide_into_folds(documents, nr_of_folds)

    total_content_result : List[int] = []
    total_environment_result : List[int] = []
    total_combined_result : List[int] = []

    total_target : List[int] = []

    documents_with_entity_in_top_ranking = 0

    for test_fold_index, test_fold in enumerate(folds):

        print('======== Fold',test_fold_index,'============')

        testing_target: List[int] = []
        training_target: List[int] = []

        training_environment_instances: List[List[bool]] = []
        testing_environment_instances: List[List[bool]] = []
        training_content_instances: List[List[bool]] = []
        testing_content_instances: List[List[bool]] = []

        if not use_cache:
            #Collect all train docs in test docs in two lists
            print('* Collecting documents')
            test_docs : List[Document] = test_fold
            train_docs : List[Document] = []

            for subfold_index, subfold in enumerate(folds):

                if not subfold_index == test_fold_index:
                    train_docs += subfold

            nr_of_train_docs: int = len(train_docs)
            nr_of_test_docs : int = len(test_docs)

            #Save the chosen entities for this fold
            print('* Save the chosen entities for this fold')
            entity_cache_file = open(cache_location+str(test_fold_index)+'.entities','w')

            for document in test_docs:
                for entity in document.entities:
                    entity_cache_file.write(document.name+' '+entity.content+'\n')

            # Create train content instances using these labels
            print('* Create train instances based on content')
            content_instance_creator: partial = partial(create_content_instances,
                                                           annotations_of_interest=annotations_to_filter)

            pool = Pool(nr_of_threads)
            for n, (target, features) in enumerate(pool.imap(content_instance_creator,train_docs)):  # type: ignore # (not supported yet in python)
                #print(' * Document', n, '/', nr_of_train_docs, 'done') #Goes fast enough
                training_target += target
                training_content_instances += features

            # Create test content instances using these labels
            print('* Create test instances based on content')

            pool = Pool(nr_of_threads)
            for n, (target, features) in enumerate(pool.imap(content_instance_creator,test_docs)):  # type: ignore # (not supported yet in python)
                #print(' * Document', n, '/', nr_of_test_docs, 'done') # Goes fast enough
                testing_target += target
                testing_content_instances += features

            #Get what would be good labels and cluster them
            print('* Automatically finding useful labels')
            label_clusters : Dict[Direction, List[Cluster]] = get_all_labels(train_docs,annotations_to_filter)

            #Create train environment instances using these labels
            print('* Create train environment instances using these labels')
            training_target = []
            testing_target = []

            environment_instance_creator : partial = partial(create_environment_instances_based_on_labels,label_clusters=label_clusters,
                                                                                            annotations_of_interest=annotations_to_filter)
            pool = Pool(nr_of_threads)
            for n, (target, features) in enumerate(pool.imap(environment_instance_creator,train_docs)): # type: ignore # (not supported yet in python)
                print(' * Document',n,'/',nr_of_train_docs,'done')
                training_target += target
                training_environment_instances += features

            #Create test environment instances using these labels
            print('* Create test environment instances using these labels')

            pool = Pool(nr_of_threads)
            for n, (target, features) in enumerate(pool.imap(environment_instance_creator,test_docs)):  # type: ignore # (not supported yet in python)
                print(' * Document',n,'/',nr_of_test_docs,'done')
                testing_target += target
                testing_environment_instances += features

            #Save created instances in cache for later reuse
            print('* Save created instances in cache')
            dump(training_target,open(cache_location+str(test_fold_index)+'.train.target.pkl','wb'))
            dump(testing_target,open(cache_location+str(test_fold_index)+'.test.target.pkl','wb'))

            dump(training_environment_instances,open(cache_location+str(test_fold_index)+'.train.env.instances.pkl','wb'))
            dump(testing_environment_instances,open(cache_location+str(test_fold_index)+'.test.env.instances.pkl','wb'))

            dump(training_content_instances,open(cache_location+str(test_fold_index)+'.train.content.instances.pkl','wb'))
            dump(testing_content_instances,open(cache_location+str(test_fold_index)+'.test.content.instances.pkl','wb'))

        else:
            print('* Loading cached instances')
            training_target = load(open(cache_location+str(test_fold_index)+'.train.target.pkl','rb'))
            testing_target = load(open(cache_location+str(test_fold_index)+'.test.target.pkl','rb'))

            training_environment_instances = load(open(cache_location+str(test_fold_index)+'.train.env.instances.pkl','rb'))
            testing_environment_instances = load(open(cache_location+str(test_fold_index)+'.test.env.instances.pkl','rb'))
            training_content_instances = load(open(cache_location + str(test_fold_index) + '.train.content.instances.pkl', 'rb'))
            testing_content_instances = load(open(cache_location + str(test_fold_index) + '.test.content.instances.pkl', 'rb'))

        entity_contents: List[str] = open(cache_location + str(test_fold_index) + '.entities').read().splitlines()

        #The actual experiments start here
        classifiers = [MLPClassifier(),KNeighborsClassifier(),SVC(probability=True),GaussianNB(),
                       RandomForestClassifier(),DecisionTreeClassifier()]

        #Content experiment
        content_results, content_confidences = traintest(KNeighborsClassifier(),'content',test_fold,training_content_instances,
                                                         testing_content_instances,training_target,testing_target)

        content_rankings = list_of_confidences_to_list_of_ranks_per_document(content_confidences,test_fold)
        content_normalized_confidences = normalize_confidences_per_document(content_confidences,test_fold)

        if ERROR_ANALYSIS:
            show_errors(content_results,content_normalized_confidences,[],testing_target,entity_contents)

        #Environment experiment
        environment_results, environment_confidences = traintest(MLPClassifier(),'environment',test_fold,training_environment_instances,
                                                                 testing_environment_instances,training_target,testing_target)

        environment_rankings = list_of_confidences_to_list_of_ranks_per_document(environment_confidences,test_fold)
        environment_normalized_confidences = normalize_confidences_per_document(environment_confidences,test_fold)

        if ERROR_ANALYSIS:
            show_errors(environment_results,[],environment_normalized_confidences,testing_target,entity_contents)

        #Combination experiment
        print('* Combining confidences')
        combined_results : List[int] = pick_entity_per_document_by_normalized_confidence(content_normalized_confidences,
                                                                                environment_normalized_confidences,test_fold)

        if ERROR_ANALYSIS:
            show_errors(combined_results,content_normalized_confidences,environment_normalized_confidences,testing_target,entity_contents)

        print('  * p', precision_score(testing_target, combined_results),
              'r', recall_score(testing_target, combined_results),
              'f', f1_score(testing_target, combined_results))

        total_content_result += list(content_results)
        total_environment_result += list(environment_results)
        total_combined_result += list(combined_results)
        total_target += list(testing_target)

        print()

    print('=============================')
    print('content')
    print('p',precision_score(total_target,total_content_result),
          'r',recall_score(total_target, total_content_result),
          'f',f1_score(total_target, total_content_result))

    print('environment')
    print('p',precision_score(total_target,total_environment_result),
          'r',recall_score(total_target, total_environment_result),
          'f',f1_score(total_target, total_environment_result))

    print('combined')
    print('p',precision_score(total_target,total_combined_result),
          'r',recall_score(total_target, total_combined_result),
          'f',f1_score(total_target, total_combined_result))

#================= Main script ========================

if __name__ == '__main__':

    DATA_ROOT : str = 'C:\\Users\\wstoop\\Desktop\\scripts\\autoconfig\\data\\'
    ANNOTATED_DATA_LOCATION : str = DATA_ROOT+'entity_csvs\\split_by_spaces\\'
    FOLDERS_TO_USE : List[str] = ['GD','GDL','GDR']
    ANNOTATIONS_TO_FILTER : List[str] = ['GD','GDL','GDR']

    USE_CACHE : bool = False
    CACHE_LOCATION : str = DATA_ROOT+'instance_cache_split_full/'

    NR_OF_THREADS : int = 6

    NR_OF_FOLDS : int = 10
    EXPERIMENT_SET_SIZE = 160

    if not USE_CACHE and input('Type y to continue and delete cache: ') != 'y':
        quit()

    #Grab all documents
    all_documents : List[Document] = load_documents(ANNOTATED_DATA_LOCATION,FOLDERS_TO_USE)

    #Only look at documents that contain the annotations we're interested in
    interesting_documents : List[Document] = []
    c : Counter = Counter()

    for document in all_documents:
        for entity in document.entities:
            if entity.annotation not in [None,'']:
                if entity.annotation in ANNOTATIONS_TO_FILTER:
                    interesting_documents.append(document)
                c[entity.annotation] += 1

    #List label distribution
    print([(i, c[i], c[i] / len(list(c.elements())) * 100.0) for i in c])

    #Run the experiment
    run_experiment(interesting_documents[:EXPERIMENT_SET_SIZE],NR_OF_FOLDS,ANNOTATIONS_TO_FILTER,USE_CACHE,CACHE_LOCATION,NR_OF_THREADS)

